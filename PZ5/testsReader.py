import unittest
from reader import answer
import reader

class TestReader(unittest.TestCase):
    def test_1(self):
        true_answer = ['brand - BMW', 'model - X5', 'submodel - M', 'produced - 2015', "species - [{'HP': 286, 'torque': 400}]", 'NAME - Night Lovell', 'AGE - 21', 'GENRE - Cloud', 'Country - Canada']
        self.assertEqual(answer[0], true_answer)
    def test_2(self):
        true_answer = ['brand - Mersedes-Benz', 'model - 200', 'submodel - SE', 'produced - 2019', 'NAME - Lord Glenarvan', 'AGE - 22', 'GENRE - Scientific', 'Country - France']
        self.assertEqual(answer[1], true_answer)
    def test_3(self):
        true_answer = ['brand - Toyota', 'model - 50', 'submodel - Camry', 'produced - 2018', "species - [{'complex': 'exlusive', 'torque': 400}]", 'NAME - Ivan Gareev', 'AGE - 41', 'Profession - Cloud', 'Country - Russia']
        self.assertEqual(answer[2], true_answer)

if __name__ == '__main__':
    reader.main()
    unittest.main()