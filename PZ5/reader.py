import json
import json.decoder as j
import xml.etree.ElementTree as ET
import unittest
import sys

answer = []

class Reader:
    def __init__(self, file_name):
        self.file_name = file_name
        self.data = []
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.current >= self.end:
            raise StopIteration

        result = self.data[self.current]
        self.current += 1
        return result


class ReaderJson(Reader):
    def read(self):
        with open(self.file_name, 'r') as file:
            try:
                data: dict = json.load(file)
            except j.JSONDecodeError as e:
                print(f"Your json-file is not correct!\nProblem is: {e}\nCheck and rewrite file please!\n")
                sys.exit()

        for key, value in data.items():
            self.data.append(f"{key} - {value}")

        self.end = len(self.data)


class ReaderXml(Reader):
    def read(self):
        with open(self.file_name, 'r') as file:
            try:
                data = ET.parse(file)
            except ET.ParseError as e:
                print(f"Your xml-file is not correct!\nProblem is: {e}\nCheck and rewrite file please!\n")
                sys.exit()

        for line in data.getroot():
            self.data.append(f"{line.tag} - {line.text}")

        self.end = len(self.data)


def main():

    for i in range(3):
        sub_answer = []
        file_name = 'file' + str(i)
        JSON_READER = ReaderJson(f"{file_name}.json")
        JSON_READER.read()

        XML_READER = ReaderXml(f"{file_name}.xml")
        XML_READER.read()

        for line in JSON_READER.__iter__():
            sub_answer.append(line)

        for line in XML_READER.__iter__():
            sub_answer.append(line)
        
        answer.append(sub_answer)
    
    print(answer)

if __name__ == "__main__":
    main()