# pylint: disable=too-few-public-methods
# pylint: disable=R0912                        # R0912: Too many branches
from math import pi
"""
program for draft figure
"""
FIGURES = []

class Figure:
    """ Class figure
    attribute:
            name
            radius
    """
    def __init__(self, name, radius, area):
        self.name = name
        self.radius = radius
        self.area = area

    def __str__(self):
        return "{0}, радиус = {1}, площадь = {2}".format(self.name, self.radius, self.area)

    def __repr__(self):
        return "Фигура {0}".format(self.name)



class Circle(Figure):
    """
    Class circle
    method:
        draft
    """

    def __init__(self, radius = 14):
        self.name = "circle"
        self.radius = radius
        area = pi * radius * radius
        self.area = area
        super().__init__("circle", radius, area)

    def draft(self):
        """
        Draft figure
        """
        radius = self.radius
        if radius < 3:
            print("Радиус должен быть больше 3")
            return
        for i in range(radius):
            if i in (0, radius - 1):
                for _ in range(radius // 2, 0, -1):
                    print(' ', end='')
                print('*', end='')
                print()
            else:
                if i < radius // 2:
                    for _ in range(radius // 2 - i, 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    for _ in range(2 * i - 1, 0, -1):
                        print(' ', end='')
                    print('*', end='')
                    print()
                elif i >= radius - radius // 2:
                    for _ in range(radius // 2 - (radius - i - 1), 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    if radius % 2 == 0:
                        for _ in range(radius - (radius // 2 - (radius - i - 1)) * 2 - 1, 0, -1):
                            print(' ', end='')
                    else:
                        for _ in range(radius - (radius // 2 - (radius - i - 1)) * 2 - 2, 0, -1):
                            print(' ', end='')

                    print('*', end='')
                    print()

        print()


class Oval(Figure):
    """
    Class oval
    method:
        draft
    """

    def __init__(self, radius = 5):
        self.radius = radius
        self.name = "oval"
        a = 1.5 * radius
        b = radius
        area = pi * a * b
        super().__init__("oval", radius, area)

    def draft(self):
        """
        Draft figure
        """
        radius = self.radius
        if radius < 3:
            print("Радиус должен быть больше 2")
            return
        for i in range(radius * 3):
            if i in (0, radius * 3 - 1):
                for _ in range(radius // 2, 0, -1):
                    print(' ', end='')
                print('*', end='')
                print()
            else:
                if i < radius // 2:
                    for _ in range(radius // 2 - i, 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    for _ in range(2 * i - 1, 0, -1):
                        print(' ', end='')
                    print('*', end='')
                    print()
                elif radius // 2 <= i < radius * 3 - radius // 2:
                    print('*', end='')
                    for _ in range(radius - 2):
                        print(' ', end='')
                    print('*', end='')
                    print()
                elif i >= radius * 3 - radius // 2:
                    for _ in range(radius // 2 - (radius * 3 - i - 1), 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    if radius % 2 == 0:
                        for _ in range(radius - (radius//2-(radius*3-i-1))*2-1, 0, -1):
                            print(' ', end='')
                    else:
                        for _ in range(radius - (radius//2-(radius*3-i-1))*2-2, 0, -1):
                            print(' ', end='')

                    print('*', end='')
                    print()

        print()


class Square(Figure):
    """
    Class oval
    method:
        draft
    """

    def __init__(self, radius = 10):
        self.name = "square"
        self.radius = radius
        area = radius * radius
        super().__init__("square", radius, area)

    def draft(self):
        """
        Draft figure
        """
        radius = self.radius
        if radius < 6:
            print("Для корекктного отображения радиус должен быть > 5")
            return
        for i in range(radius // 2):
            if i in (0, radius // 2 - 1):
                for _ in range(radius):
                    print('*', end='')
            else:
                print('*', end='')
                for _ in range(1, radius - 1):
                    print(' ', end='')
                print('*', end='')
            print()


def main():

    OVAL = Oval()
    print(OVAL)
    OVAL.draft()
    FIGURES.append(OVAL)

    CIRCLE = Circle()
    print(CIRCLE)
    CIRCLE.draft()
    FIGURES.append(CIRCLE)

    SQUARE = Square()
    print(SQUARE)
    SQUARE.draft()
    FIGURES.append(SQUARE)

    print(FIGURES)

if __name__ == "__main__":
    main()