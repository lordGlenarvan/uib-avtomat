import unittest
from figure import FIGURES
import figure

class TestFigure(unittest.TestCase):
    def test_1(self):
        true_answer = 117.80972450961724
        self.assertEqual(FIGURES[1].oval.area, true_answer)
    def test_2(self):
        true_answer = 615.7521601035994
        self.assertEqual(FIGURES[1].circle.area, true_answer)
    def test_3(self):
        true_answer = 100
        self.assertEqual(FIGURES[1].square.area, true_answer)

if __name__ == '__main__':
    figure.main()
    unittest.main()