# pylint: disable=too-few-public-methods
# pylint: disable=R0912                        # R0912: Too many branches
from math import pi
"""
program for draft figure
"""
FIGURES = []

class Circle():
    """
    Class circle
    method:
        draft
    """

    def __init__(self, radius = 14):
        self.name = "circle"
        self.radius = radius
        self.area = pi * radius * radius

    def draft(self):
        """
        Draft figure
        """
        radius = self.radius
        if radius < 3:
            print("Радиус должен быть больше 3")
            return

        for i in range(radius):
            if i in (0, radius - 1):
                for _ in range(radius // 2, 0, -1):
                    print(' ', end='')
                print('*', end='')
                print()
            else:
                if i < radius // 2:
                    for _ in range(radius // 2 - i, 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    for _ in range(2 * i - 1, 0, -1):
                        print(' ', end='')
                    print('*', end='')
                    print()
                elif i >= radius - radius // 2:
                    for _ in range(radius // 2 - (radius - i - 1), 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    if radius % 2 == 0:
                        for _ in range(radius - (radius // 2 - (radius - i - 1)) * 2 - 1, 0, -1):
                            print(' ', end='')
                    else:
                        for _ in range(radius - (radius // 2 - (radius - i - 1)) * 2 - 2, 0, -1):
                            print(' ', end='')

                    print('*', end='')
                    print()
        print()

class Oval():
    """
    Class oval
    method:
        draft
    """

    def __init__(self, radius = 5):
        self.radius = radius
        self.name = "oval"
        a = 1.5 * radius
        b = radius
        self.area = pi * a * b

    def draft(self):
        """
        Draft figure
        """
        radius = self.radius
        if radius < 3:
            print("Радиус должен быть больше 2")
            return

        for i in range(radius * 3):
            if i in (0, radius * 3 - 1):
                for _ in range(radius // 2, 0, -1):
                    print(' ', end='')
                print('*', end='')
                print()
            else:
                if i < radius // 2:
                    for _ in range(radius // 2 - i, 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    for _ in range(2 * i - 1, 0, -1):
                        print(' ', end='')
                    print('*', end='')
                    print()
                elif radius // 2 <= i < radius * 3 - radius // 2:
                    print('*', end='')
                    for _ in range(radius - 2):
                        print(' ', end='')
                    print('*', end='')
                    print()
                elif i >= radius * 3 - radius // 2:
                    for _ in range(radius // 2 - (radius * 3 - i - 1), 0, -1):
                        print(' ', end='')
                    print('*', end='')

                    if radius % 2 == 0:
                        for _ in range(radius - (radius//2-(radius*3-i-1))*2-1, 0, -1):
                            print(' ', end='')
                    else:
                        for _ in range(radius - (radius//2-(radius*3-i-1))*2-2, 0, -1):
                            print(' ', end='')

                    print('*', end='')
                    print()
        print()

class Square():
    """
    Class oval
    method:
        draft
    """

    def __init__(self, radius = 10):
        self.name = "square"
        self.radius = radius
        self.area = radius * radius

    def draft(self):
        """
        Draft figure
        """
        radius = self.radius
        if radius < 6:
            print("Для корекктного отображения радиус должен быть > 5")
            return
        for i in range(radius // 2):
            if i in (0, radius // 2 - 1):
                for _ in range(radius):
                    print('*', end='')
            else:
                print('*', end='')
                for _ in range(1, radius - 1):
                    print(' ', end='')
                print('*', end='')
            print()

class Figures:
    """ Class figure
    attribute:
            id
            circle
            oval
            square
            figures
    """
    def __init__(self, id, circle = None, oval = None, square = None):
        self.id = id
        self.flag = 0
        self.current = 0
        self.circle = circle
        self.oval = oval
        self.square = square
        self.figures = []
        if self.circle != None:
            self.figures.append(self.circle)
            self.flag = self.flag + 1
        if self.oval != None:
            self.figures.append(self.oval)
            self.flag = self.flag + 1
        if self.square != None:
            self.figures.append(self.square)
            self.flag = self.flag + 1

    def __iter__(self):    
        return self

    def __next__(self):
        if self.current >= self.flag:
            raise StopIteration

        result = self.figures[self.current]
        self.current += 1
        return result

    def __str__(self):
        s = "I have: "
        if self.circle != None:
            s = s + f"[{self.circle.name}, радиус = {self.circle.radius}, площадь = {self.circle.area}] "
        if self.oval != None:
            s = s + f"[{self.oval.name}, радиус = {self.oval.radius}, площадь = {self.oval.area}]] "
        if self.square != None:
            s = s + f"[{self.square.name}, радиус = {self.square.radius}, площадь = {self.square.area}]] "
        if self.flag == 0:
            s = s + "No figures!"
        return s

    def __repr__(self):
        return f"figure {self.id} - {self.__str__()}\n"


def main():
    
    OVAL = Oval()
    CIRCLE = Circle()
    SQUARE = Square()
    for i in range(3):
        if i % 2 == 1:
            FIGURE = Figures(i, CIRCLE, OVAL, SQUARE)
        else:
            FIGURE = Figures(i, CIRCLE, OVAL)
        FIGURES.append(FIGURE)
    
    FIGURES.append(Figures(3))
 
    print(FIGURES)
    for f in FIGURES[1]:
        f.draft()

if __name__ == "__main__":
    main()