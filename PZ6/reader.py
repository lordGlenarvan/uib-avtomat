import json
import xml.etree.ElementTree as ET
import unittest
import sys
import json as j

answer = []

class ReaderJson():
    def __init__(self, file_name):
        self.file_name = file_name
        self.data = []

    def read(self):
        with open(self.file_name, 'r') as file:
            try:
                data: dict = json.load(file)
            except j.JSONDecodeError as e:
                print(f"Your json-file is not correct!\nProblem is: {e}\nCheck and rewrite file please!\n")
                sys.exit()

        for key, value in data.items():
            self.data.append(f"{key} - {value}")

        self.end = len(self.data)


class ReaderXml():
    def __init__(self, file_name):
        self.file_name = file_name
        self.data = []

    def read(self):
        with open(self.file_name, 'r') as file:
            try:
                data = ET.parse(file)
            except ET.ParseError as e:
                print(f"Your xml-file is not correct!\nProblem is: {e}\nCheck and rewrite file please!\n")
                sys.exit()

        for line in data.getroot():
            self.data.append(f"{line.tag} - {line.text}")

        self.end = len(self.data)

class Reader:
    def __init__(self, json = None, xml = None):
        self.data = []
        tmp = []
        self.flag = 0
        self.current = 0
        self.json = json
        self.xml = xml
        if self.json != None:
            tmp.append(self.json.data)

        if self.xml != None:
            tmp.append(self.xml.data)
        
        self.data.append(tmp)
        self.flag = self.flag + 1

    def __iter__(self):
        return self

    def __next__(self):
        if self.current >= self.flag:
            raise StopIteration

        result = self.data[self.current]
        self.current += 1
        return result


def main():
    for i in range(3):
        sub_answer = []
        file_name = 'file' + str(i)
        JSON_READER = ReaderJson(f"{file_name}.json")
        JSON_READER.read()

        XML_READER = ReaderXml(f"{file_name}.xml")
        XML_READER.read()

        READER = Reader(JSON_READER, XML_READER)

        for line in READER.__iter__():
            answer.append(line)

    print(answer)

if __name__ == "__main__":
    main()